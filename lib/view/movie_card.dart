import 'package:flutter/material.dart';
import 'package:movies_ap/model/movie.dart';

class MovieCard extends StatelessWidget {
  Movie value;
  OnMovieChangeListener valueChanged;

  MovieCard(this.value, this.valueChanged);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      child: Center(
        child: Container(
          margin: EdgeInsets.all(1.0),
          height: 210,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(1)),
          child: Stack(
            children: [
              Align(alignment: Alignment.topCenter, child:createCard())
            ],
          ),
        ),
      ),
    );
  }


  Widget createCard() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[createGridCard(this.value),createBanner()
      ],
    );
  }

  Widget createBanner() {
    return Padding(
      padding: EdgeInsets.all(1.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Align(
            alignment: Alignment.center,
            child: Text(this.value?.title ?? '',maxLines: 4,style: TextStyle(color: Colors.black),),
          )
        ],
      ),

    );
  }


  Widget createCardBoder() {
    return Container(
      width: 143,
      height: 210,
      child: createCard(),
    );
  }

  Widget createGridCard(final Movie movie) {
    return Container(
      width: 143,
      height: 200,

      child: Stack(

        children: [
          Align(
            alignment: Alignment.center,
            child: Container(
              width: 143,
              height: 210,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/movie.png'),
                  fit: BoxFit.fill,
                )
              ),
            )
          )
        ],
      ),

    );
  }
}

abstract class OnMovieChangeListener {
  void onMovieValueChange(Movie movie);
}