import 'package:flutter/material.dart';
import 'package:movies_ap/model/movie.dart';
import 'package:movies_ap/view/base_widget.dart';
import 'package:movies_ap/view/movie_card.dart';
import 'package:movies_ap/viewmodel/movieViewModel.dart';
import 'package:provider/provider.dart';

class MyHomePage extends BaseWidget implements OnMovieChangeListener {
  MovieViewModel movieViewModel;



  @override
  Widget buildChild(BuildContext context) {
    Provider.of<MovieViewModel>(context,listen: false).getAllMovies();
    return Scaffold(
        appBar: AppBar(
          title: Text("Home"),
        ),
        body: Consumer<MovieViewModel>(
          builder: (context,movie,child)=>GridView.count(
            crossAxisCount: 2,
            shrinkWrap: true,
            padding: EdgeInsets.only(left:1.0,right: 1.0,bottom: 1.0),
            children: movie.movieList.map((e) => MovieCard(e,this)).toList(),
          ),
        )
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }


  @override
  void onMovieValueChange(Movie movie) {
    // TODO: implement onMovieValueChange
  }
}







