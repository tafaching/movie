import 'package:flutter/cupertino.dart';
import 'package:movies_ap/model/movie.dart';
import 'package:movies_ap/network/generic_repo.dart';

class MovieViewModel extends ChangeNotifier with AMovieViewModel {
  String allMovies = '';

  List<Movie> movieList = [];

  @override
  void getAllMovies() async {
    await GenericRepo<Movie>()
        .getAll(allMovies, Movie(), null, null)
        .then((response) {
      movieList = response;
    });
    notifyListeners();
  }
}

abstract class AMovieViewModel {
  void getAllMovies();
}
