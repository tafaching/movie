import 'package:dio/dio.dart';

import 'dio_client.dart';

Future getMovies(String endpoint, {Map params}) async {
  Response response =
      await DioClient.getDioClient().get(endpoint, queryParameters: params);

  return response?.data;
}
