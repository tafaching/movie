import 'package:movies_ap/model/base_model.dart';
import 'package:movies_ap/network/json_factory.dart';

import 'http_remote_service.dart';

class GenericRepo<T extends ModelMapper> {
  Future<List<T>> getAll(
      String endpoint, T responseType, Map queryParams, Map pathVariable) {
    return getMovies(builderUrl(endpoint, pathVariable), params: queryParams)
        .then((response) {
      return JsonEncoderMapper<T>().mapToList(responseType, response);
    });
  }

  String builderUrl(String endpoint, Map pathVariable) {
    if (pathVariable != null) {
      for (MapEntry mapEntry in pathVariable.entries) {
        endpoint = endpoint.replaceAll(
            '{' + mapEntry.key + '}', mapEntry.value.toString());
      }
    }
    return endpoint;
  }
}
