import 'package:movies_ap/model/base_model.dart';

class JsonEncoderMapper<T extends ModelMapper> {

  List<T> mapToList(T classType, dynamic response) {
    List<T> responseList = [];
    var json = response as List;

    for (var item in json) {
      var map = item as Map;
      responseList.add(classType.newInstance().fromMap(Map));
    }
    return responseList;
  }
//  return responseList;
}