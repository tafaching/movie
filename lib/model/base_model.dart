abstract class ModelMapper{
  ModelMapper fromMap(dynamic map);

  Map toJson();

  ModelMapper newInstance();
}