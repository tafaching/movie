import 'package:movies_ap/model/base_model.dart';

class Movie with ModelMapper {
  int id;
  String title;
  String year;
  String cast;
  String description;
  String image;
  int rating;

  Movie(
      {this.title,
      this.year,
      this.cast,
      this.description,
      this.image,
      this.id,
      this.rating});

  @override
  ModelMapper newInstance() {
    return Movie();
  }

  @override
  ModelMapper fromMap(map) {
    id = map['id'];
    title = map['title']?.toString() ?? '';
    year = map['year']?.toString() ?? '';
    cast = map['cast']?.toString() ?? '';
    description = map['description']?.toString() ?? '';
    image = map['image']?.toString() ?? '';
    rating = map['rating'];
    return this;
  }

  Map toJson() {
    return {
      'title': title,
      'year': year,
      'cast': cast,
      'description': description,
      'image': image,
      'rating': rating
    };
  }
}
